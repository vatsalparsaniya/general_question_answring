import json
import logging
import uuid
from typing import Callable

from starlette.responses import Response

from fastapi import Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi.routing import APIRoute

from app.config import settings

logger = logging.getLogger("general_question_answering")


def handle_request_validation_error(request, exc):
    try:
        validation_message = json.loads(exc.json())[0]
        validation_message = validation_message.get("msg", "")
    except:
        validation_message = str(exc)
    response = {
        "data": None,
        "message": f"invalid_input: {validation_message}",
        "status": 422,
    }
    return JSONResponse(status_code=422, content=response)


def handle_unexpected_errors(request, exc):
    response = {"data": None, "message": str(exc), "status": 500}
    return JSONResponse(status_code=500, content=response)


def add_custom_headers(response):
    # response.headers['Content-Security-Policy'] = "default-src 'self'"
    response.headers[
        "Strict-Transport-Security"
    ] = "max-age=31536000; includeSubDomains"
    response.headers["X-Content-Type-Options"] = "nosniff"
    response.headers["X-Frame-Options"] = "SAMEORIGIN"
    response.headers["X-XSS-Protection"] = "1; mode=block"
    response.headers["Cache-Control"] = "no-cache, no-store"
    response.headers["Pragma"] = "no-cache"
    return response


class CustomLoggerRoute(APIRoute):
    """ Custom Route middleware implementation for logging request and correspoinding response"""

    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            request_payload = None
            if request.method == "POST":
                request_payload = await request.json()
            request_id = str(uuid.uuid4())
            request_logger_json = {
                "env": "dev",
                "type": "request",
                "httpMethod": request.method,
                "path": request.url.path,
                "userId": "",
                "threadId": "",
                "requestId": request_id,
                "parameters": "",
                "requestBody": request_payload,
                "requestHeaders": dict(request.headers.items()),
                "serviceName": settings.SERVICE_NAME,
            }
            try:
                response: Response = await original_route_handler(request)
            except RequestValidationError as error:
                response = handle_request_validation_error(request, error)
            except Exception as error:
                response = handle_unexpected_errors(request, error)
            finally:
                # logger.info(json.dumps(request_logger_json))
                response_logger_json = {
                    "env": settings.APP_ENVIRONMENT,
                    "type": "response",
                    "httpMethod": request.method,
                    "path": request.url.path,
                    "userId": "",
                    "threadId": "",
                    "requestId": request_id,
                    "responseStatus": response.status_code,
                    "responseBody": json.loads(response.body),
                    "responseStatusStr": str(response.status_code),
                    "responseHeaders": dict(request.headers.items()),
                    "serviceName": settings.SERVICE_NAME,
                }
                # if 200 <= response.status_code < 400:
                #     logger.info(json.dumps(response_logger_json))
                # else:
                #     logger.error(json.dumps(response_logger_json))
            response = add_custom_headers(response)
            return response

        return custom_route_handler
