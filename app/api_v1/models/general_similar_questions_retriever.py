from typing import List, Union

from pydantic import BaseModel, Field, validator


class GeneralSimilarQuestionRetrievalRequestModel(BaseModel):
    question_text: Union[str, None] = Field(description="Question text")
    top_k: int = Field(description="Number of result to retrieve")

    @validator("question_text")
    def validate_question_text(cls, value):
        """ Validate the question text"""
        if isinstance(value, (str,)) and not len(value) > 0:
            raise ValueError("'question_text' is too short")
        return value

    @validator("top_k")
    def validate_question_count(cls, value):
        """ Validator to check the k value"""
        if not 0 < value <= 10:
            raise ValueError("'top_k' value in request should be in between 1 to 10")
        return value

    class Config:
        schema_extra = {
            "example": {
                "question_text": "What is EMBIBE?",
                "top_k": 5,
            }
        }


class PredictedQuestionModel(BaseModel):
    cosine_similarity: int = Field(description="Similarity score for the question")
    question: str = Field(description="question text")
    question_code: str = Field(description="question code")
    answer: str = Field(description="answer of question")


class RetrievedGeneralSimilarQuestions(BaseModel):
    questions: List[PredictedQuestionModel] = Field(
        description="List of predicted similar questions"
    )


class GeneralSimilarQuestionRetrievalResponseModel(BaseModel):
    data: Union[RetrievedGeneralSimilarQuestions, None]
    message: str = Field(..., example="success")
    status: Union[str, int] = Field(..., example=200)

    class Config:
        schema_extra = {
            "example": {
                "data": {
                    "questions": [
                        {
                            "cosine_similarity": 1.0,
                            "question": "What is EMBIBE?",
                            "question_code": "KA_FAQ_Student_15_1",
                            "answer": "The story behind Embibe is one that every student around the world ..."
                        },
                        {
                            "cosine_similarity": 0.93162215,
                            "question": "How does EMBIBE work?",
                            "question_code": "KA_FAQ_Student_15_3",
                            "answer": "The story behind Embibe is one that every student around the world ..."
                        },
                    ]
                },
                "message": "success",
                "status": 200,
            }
        }
