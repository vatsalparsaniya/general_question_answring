from fastapi import APIRouter

from app.api_v1.endpoints.general_similar_questions_retriever import router as gqr_router
from app.api_v1.endpoints.healthcheck import router as hc_router

router = APIRouter()

__all__ = ["router"]

router.include_router(hc_router, tags=["healthcheck"])
router.include_router(gqr_router, tags=["general_question_answering"])
