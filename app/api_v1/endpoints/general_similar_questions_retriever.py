import httpx
import logging
from fastapi import APIRouter, Depends, Response, status
from app.core.middlewares import CustomLoggerRoute
from app.api_v1.models.general_similar_questions_retriever import (
    GeneralSimilarQuestionRetrievalRequestModel,
)
from app.utils.similar_questions_retriever import get_similar_questions

LOGGER = logging.getLogger("general_question_answering")

router = APIRouter(route_class=CustomLoggerRoute)


@router.post(
    "/retrieve-similar-questions"
)
async def predict_general_similar_questions(
        request_data: GeneralSimilarQuestionRetrievalRequestModel,
        response: Response,
):
    """ Returns the similar questions for the given question text"""
    response_body = {}
    try:
        similar_questions = await get_similar_questions(question_text=request_data.question_text,
                                                        top_k=request_data.top_k)
        response.status_code = status.HTTP_200_OK
        response_body.update({
            "data": {
                "questions": similar_questions
            },
            "message": "successful",
            "status": response.status_code})

    except Exception as error:
        error_message = "unexpected error occured in similar question retrieval process"
        LOGGER.exception("%s :: %s", error_message, str(error))
        response_body.update({"data": None, "message": error_message, "status": 500})
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    return response_body
