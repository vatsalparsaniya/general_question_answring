import os

APP_ENVIRONMENT = os.getenv("APP_ENVIRONMENT", "dev")

SERVICE_NAME = "general_question_answering"
# The base directory of the application.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Semantic versioning of the application.
VERSION = ("1", "0", "0")

# Add application specific settings here.

# Logging configuration
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "short": {
            "class": "logging.Formatter",
            "format": "%(levelname)-s: %(message)s",
        },
        "long": {
            "class": "logging.Formatter",
            "format": SERVICE_NAME
            + ":%(asctime)s %(name)s: %(levelname)s: %(message)s",
        },
    },
    "handlers": {
        "file": {
            "class": "logging.handlers.RotatingFileHandler",
            "level": "DEBUG",
            "formatter": "long",
            "filename": f"logs/{SERVICE_NAME}.log",
            "backupCount": 10,
            "maxBytes": 50000000,
        },
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "short",
            "stream": "ext://sys.stdout",
        },
        "syslog": {
            "class": "logging.handlers.SysLogHandler",
            "level": "INFO",
            "address": "/dev/log",
            "formatter": "long",
        },
    },
    "loggers": {
        f"{SERVICE_NAME}": {
            "level": "INFO",
            "handlers": ["console"] if APP_ENVIRONMENT == "stage" else ["console"],
            "propagate": 0,
        }
    },
}