from typing import List
import numpy as np
import onnxruntime
from transformers import DPRQuestionEncoderTokenizer
from elasticsearch import Elasticsearch

es_client = Elasticsearch("10.141.2.190:9200", timeout=600)
tokenizer = DPRQuestionEncoderTokenizer.from_pretrained("bert-base-uncased", truncation=True, max_length=512)
dpr_model = onnxruntime.InferenceSession("/home/devops/medhas/darshan/model_dir/dpr-hf.onnx")


def get_es_query(user_question_embedding: List[float], top_k: int = 5) -> dict:
    search_query = {
        "size": top_k,
        "_source": ["question", "question_code", "answer"],
        "query": {
            "script_score": {
                "query": {
                    "match_all": {}
                },
                "script": {
                    "source": "cosineSimilarity(params.user_question_embedding, doc['question_dpr_dense_vec'])",
                    "params": {"user_question_embedding": user_question_embedding}
                }
            }
        }
    }
    return search_query


def get_clean_text(text: str) -> str:
    text = text.lower()
    return text


def get_dpr_embeddings(text_list: list,
                       preprocessing: bool = True,
                       batch_size: int = 32) -> np.array:
    if preprocessing:
        text_list = [get_clean_text(text) for text in text_list]

    text_embeddings = []
    for i in range(0, len(text_list), batch_size):
        encoded_tokens = tokenizer(text_list[i:i + batch_size],
                                   truncation=True,
                                   max_length=512,
                                   padding=True)

        encoded_input = {
            "input_ids": encoded_tokens["input_ids"],
            "attention_mask": encoded_tokens["attention_mask"],
            "token_type_ids": encoded_tokens["token_type_ids"],
        }

        dpr_model_output = dpr_model.run(None, encoded_input)
        text_embeddings.extend(dpr_model_output[0])

    return np.vstack(text_embeddings)


async def get_similar_questions(question_text: str, top_k: int = 5) -> list:
    question_embedding = get_dpr_embeddings([question_text])
    es_query = get_es_query(user_question_embedding=question_embedding[0],
                            top_k=top_k)

    response = es_client.search(
        index="general_question_embedding",
        body=es_query
    )

    similar_question_data = list()
    if response['hits']['total']['value']:
        for hits in response['hits']['hits']:
            _source = hits['_source']
            _score = hits['_score']

            similar_question_data.append({**_source, **{"cosine_similarity": _score}})

    return similar_question_data
