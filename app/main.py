import os
import sys
from collections import namedtuple
from app.api_v1.endpoints import router as app_v1
from app.core.utils import setup_application

SubApp = namedtuple("SubApp", ["prefix", "router"])

# Add all the SubApp for registration
sub_applications = [SubApp("/api/v1", app_v1)]

app = setup_application(
    title="general_question_answering",
    description="This is the default project description for general_question_answering.",
    sub_applications=sub_applications,
)
