import os
import sys

import click

import pytest
import uvicorn

# Append the application directory to sys.path
APP_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "app")
sys.path.append(APP_DIR)

APP_PORT = 8686


@click.group()
def cli():
    pass


@cli.command()
def runserver():
    """
    Start the debug server using uvicorn
    """
    uvicorn.run(
        "app.main:app",
        host="0.0.0.0",
        port=APP_PORT,
        log_level="debug",
        reload=True,
        reload_dirs=["app"],
    )


@cli.command()
def test():
    """
    Unittest runner.
    """
    pass
    # pytest_args = ["--cov=app", "--cov-report=term-missing", "--cov-fail-under=95"]
    # exit_code = pytest.main(pytest_args)
    # raise SystemExit(exit_code.value)


if __name__ == "__main__":
    cli()
